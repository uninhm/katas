def canPlaceFlowers(parterre, n):
    free_plots = 0
    for i in range(len(parterre)):
        if parterre[i] == 0 and (i == 0 or parterre[i-1] == 0) and (i == len(parterre)-1 or parterre[i+1] == 0):
            parterre[i] = 1
            free_plots += 1
    return n <= free_plots
