'''Supongamos que tienes un largo parterre en el que algunas de las parcelas están plantadas y otras no. 
Sin embargo, las flores no pueden ser plantadas en parcelas adyacentes - competirían por el agua y ambas morirían.
Dado un parterre (representado como una matriz que contiene 0 y 1, donde 0 significa vacío y 1 significa no vacío), 
y un número n, regrese si se pueden plantar n nuevas flores en él sin violar la regla de no-adjuntar flores.'''

# Parterre sera una lista con ceros y unos exclusivamente, y n un numero entero positivo que no excederá el tamaño de la matriz de entrada, return True o False
# Escribe los comentarios necesarios para favorecer el entendiemiento del programa

def canPlaceFlowers(parterre, n):
    # escribe aqui en código de tu solución
