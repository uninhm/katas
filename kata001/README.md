### Enunciado

Supongamos que tienes un largo parterre en el que algunas de las parcelas están plantadas y otras no. 
Sin embargo, las flores no pueden ser plantadas en parcelas adyacentes - competirían por el agua y ambas morirían.
Dado un parterre (representado como una matriz que contiene 0 y 1, donde 0 significa vacío y 1 significa no vacío), 
y un número n, regrese si se pueden plantar n nuevas flores en él sin violar la regla de no-adjuntar flores.
